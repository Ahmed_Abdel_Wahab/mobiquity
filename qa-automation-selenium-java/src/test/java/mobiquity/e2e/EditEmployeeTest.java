package mobiquity.e2e;
import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import baseTest.ValidLoginTest;
import pagesobjectsmodel.EmployeeEdit;
import utility.PropertyEmployeeManager;

//PreConditions for this test case are execution of : 
//1)Setup , 2)ValidLoginTest	
public class EditEmployeeTest extends ValidLoginTest {
	@Before
	@Override
	public void testValidLogin() throws Exception {
		super.testValidLogin();
	}
	
//Create Object from Edit page class		
	pagesobjectsmodel.EmployeeEdit objEmployeeEdit;
	PropertyEmployeeManager objData;	

//************Test case 4 ' . Employee data can be changed by clicking on Edit '	
	@Test
	public void testEditEmployee() throws Exception {

//Initialize and parse driver instance to classes	
		objData = new PropertyEmployeeManager(driver);
		objEmployeeEdit = new EmployeeEdit(driver);
//Generate unique firstname and last name at propertyEmployeeManager and send value into Employee
		objData.setPropertyParamChanges();
//Get EmployeeName from home listing page
		objEmployeeEdit.Get_FirstEmployee();		
//Edit Employee
		objEmployeeEdit.Edit();	
//Edit and clear employee First and last name
		objEmployeeEdit.Clear_Employee_FirstName_LastName();
//Update employee with new first and last name
		objEmployeeEdit.Update();
//Wait until redirect to home page and assert.
        Thread.sleep(1000);
		assertTrue("Verification failed: User hasn't been added",objEmployeeEdit.Displayed_Greetings());

	}
	@After
	@Override
	public void tearDownAfterTestClass() throws Exception {
		super.tearDownAfterTestClass();
	}
	
}
