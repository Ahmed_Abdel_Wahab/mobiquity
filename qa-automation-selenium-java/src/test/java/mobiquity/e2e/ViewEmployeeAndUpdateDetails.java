package mobiquity.e2e;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import baseTest.ValidLoginTest;
import pagesobjectsmodel.EmployeeViewAndUpdate;
import utility.PropertyEmployeeManager;

public class ViewEmployeeAndUpdateDetails extends ValidLoginTest {	
	//PreConditions for this test case are execution of : 
	//1)Setup , 2)ValidLoginTest	
	@Before
	@Override
	public void testValidLogin() throws Exception {
		super.testValidLogin();
	}
//Create Object from Edit page class		
		pagesobjectsmodel.EmployeeViewAndUpdate objEmployeeViewAndUpdate;
		PropertyEmployeeManager objData;	
	@Test
//************Test case 2 ' Details about each employee can be viewed by double clicking on the employee name '
	public void testEmployeeAccessAndView() throws Exception {		
		
		objData = new PropertyEmployeeManager(driver);
		objEmployeeViewAndUpdate = new EmployeeViewAndUpdate(driver);
		
//Generate unique firstname and last name at propertyEmployeeManager and send value into Employee
		objData.setPropertyParamChanges();
//Get EmployeeName from home listing page
		objEmployeeViewAndUpdate.Get_FirstEmployee();
//Perform double click action
		objEmployeeViewAndUpdate.Edit();
//Clear employee name
		objEmployeeViewAndUpdate.Clear_Employee_FirstName_LastName();
//Update employee with new data 
		objEmployeeViewAndUpdate.Update();		
//wait until redirect to home page
		Thread.sleep(1000);	
//Assert after redirect to home page
		assertTrue("Verification failed: User hasn't been added",objEmployeeViewAndUpdate.Displayed_Greetings());

	}
	@After
	@Override
	public void tearDownAfterTestClass() throws Exception {
		super.tearDownAfterTestClass();
	}
	
	
}
