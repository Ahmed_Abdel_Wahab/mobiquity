package mobiquity.e2e;
import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import baseTest.ValidLoginTest;
import pagesobjectsmodel.EmployeeAdd;
import utility.PropertyEmployeeManager;

//PreConditions for this test case are execution of : 
//1)Setup , 2)ValidLoginTest
public class AddEmployeeTest extends ValidLoginTest {
	
	@Before
	public void testValidLogin() throws Exception {
		super.testValidLogin();
	}
	
//Create Object from Delete page class	
	pagesobjectsmodel.EmployeeAdd objEmployeeAdd;
	PropertyEmployeeManager objData;	

//************Test case 3 '  Employees can be added to the list of employees 
//by adding their first and last names, start date (format YYYY-MM-DD) and email 
	@Test
	public void testAddEmployee() throws Exception {

		
//Initialize and parse driver instance to classes	
		objData = new PropertyEmployeeManager(driver);
		objEmployeeAdd = new EmployeeAdd(driver);
//Generate unique firstname and last name at propertyEmployeeManager and send value into Employee
		objData.setPropertyParamChanges();
//Add employee
		objEmployeeAdd.AddEmployee();	
//Send employee info and create
		objEmployeeAdd.Employee_Create();
//Assert if user created and redirected to home page
		Thread.sleep(1000);
		assertTrue("Verification failed: User hasn't been added",objEmployeeAdd.Displayed_Greetings());
	}
	@After
	@Override
	public void tearDownAfterTestClass() throws Exception {
		super.tearDownAfterTestClass();
	}
	
}
