package mobiquity.e2e;
import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import baseTest.ValidLoginTest;
import pagesobjectsmodel.EmployeeLogout;

//PreConditions for this test case are execution of : 
//1)Setup , 2)ValidLoginTest	
public class LogoutTest extends ValidLoginTest {
	@Before
	@Override
	public void testValidLogin() throws Exception {
		super.testValidLogin();
	}
	
	pagesobjectsmodel.EmployeeLogout objEmployeeLogout;

//************Test case 6 ' Users can logout from the application  '
	@Test
	public void testLogout() throws Exception {
//Initialize and parse driver instance to classes	
		objEmployeeLogout = new EmployeeLogout(driver);
//Logout 
		objEmployeeLogout.Logout();
//Wait until logout		
		Thread.sleep(1000);
//Verify and assert if logged out
		assertTrue("Verification failed: User hasn't Logout",objEmployeeLogout.IsUserDisplayed());
	}
	@After
	@Override
	public void tearDownAfterTestClass() throws Exception {
		super.tearDownAfterTestClass();
	}
}
