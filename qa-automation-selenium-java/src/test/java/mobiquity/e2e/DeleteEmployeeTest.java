package mobiquity.e2e;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import baseTest.ValidLoginTest;
import pagesobjectsmodel.EmployeeDelete;

//PreConditions for this test case are execution of : 
//1)Setup , 2)ValidLoginTest
public class DeleteEmployeeTest extends ValidLoginTest {	
	@Before
	@Override
	public void testValidLogin() throws Exception {
		super.testValidLogin();
	}
//Create Object from Delete page class	
	pagesobjectsmodel.EmployeeDelete objEmployeeDelete;
	
//************Test case 5 '  Employee data can be deleted by clicking on Delete '	
	@Test	
	public void testEmployeeDelete() throws Exception {
		
//Initialize and parse driver instance to classes			
		objEmployeeDelete = new EmployeeDelete(driver);
//Click on first employee	
		objEmployeeDelete.Check_FirstEmployee();
//Delete first employee
		objEmployeeDelete.Delete();
//Wait until pop up shown 'No work around but waiting for Popup using thread.sleep 'not recommended''
		Thread.sleep(500);
//Confirm delete
		driver.switchTo().alert().accept();
//wait until refresh menu after delete
		Thread.sleep(2000);
//Assert if employee is deleted
		assertTrue("Verification failed: User hasn't been deleted",objEmployeeDelete.CheckIfDeleted());
	}
	
	@After
	@Override
	public void tearDownAfterTestClass() throws Exception {
		super.tearDownAfterTestClass();
	}

	
}

