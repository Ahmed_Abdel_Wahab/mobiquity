package baseTest;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import pagesobjectsmodel.UserLogin;
import utility.PreConditionSetup;

public class ValidLoginTest extends PreConditionSetup {

	pagesobjectsmodel.UserLogin objLogin;


    @Before
	@Override
	public void setUpBeforeTestClass() throws Exception {
		super.setUpBeforeTestClass();
	}
		
	public void testValidLogin() throws Exception {
		
		objLogin = new UserLogin(driver);
		
//************Test case 1 ' The application allows you to login with a username and a password '
//Login using saved credentials in LoginData.properties		
		objLogin.Login();
//Get greeting message after login		
		objLogin.Get_Greetings();
		Thread.sleep(1000);
//Assert and verify
		assertTrue("Verification failed: Can't verify that user has logged in",objLogin.Get_Greetings().contains(properties.getProperty("username")));

	}
	@After
	@Override
	public void tearDownAfterTestClass() throws Exception {
		super.tearDownAfterTestClass();
	}
	
	
}
