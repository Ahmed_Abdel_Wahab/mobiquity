package pagesobjectsmodel;
import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EmployeeDelete {
		
    public WebDriver driver;
    public EmployeeDelete(WebDriver driver) {
        this.driver = driver;
	      	}
    
//**Delete Employee elements 
     By Delete = By.id("bDelete");
     By FirstEmployee = By.cssSelector("li.ng-scope:nth-child(1)");
     
//**Delete employee class 'Test case ' methods
     
//Click on first employee    
	public void Check_FirstEmployee(){
		
		WebDriverWait wait = new WebDriverWait(driver, 5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(FirstEmployee)).click();
    }
	
//Get first employee data    	
	public String Get_FirstEmployee(){
		
		WebDriverWait wait = new WebDriverWait(driver, 2000);
		String employee =wait.until(ExpectedConditions.visibilityOfElementLocated(FirstEmployee)).getText();
		return employee;
    }

//Click on delete button
	public void Delete(){
		
		WebDriverWait wait = new WebDriverWait(driver, 3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Delete)).click();
    }
	
//Assert if employee deleted	
	public boolean CheckIfDeleted(){
		WebDriverWait wait = new WebDriverWait(driver, 5000);
		if (Get_FirstEmployee().equals(wait.until(ExpectedConditions.visibilityOfElementLocated(FirstEmployee)).getText()))
		{return true;}
		else
		{return false;}
    }
}
