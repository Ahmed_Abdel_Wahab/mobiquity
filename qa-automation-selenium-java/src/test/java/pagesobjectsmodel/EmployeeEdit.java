package pagesobjectsmodel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EmployeeEdit {
		
    public WebDriver driver;
    public EmployeeEdit(WebDriver driver) {
        this.driver = driver;
	      	}
    
	Properties employee = new Properties();
	
	By greetings=By.id("greetings");
//**Update and edit Employee elements
    By FirstEmployee = By.cssSelector("li.ng-scope:nth-child(1)"); 
    By AddEmployee= By.id("bAdd");
    By Employee_FirstName = By.xpath("//input[(@ng-model='selectedEmployee.firstName')]");   
    By Employee_LastName = By.xpath("//input[(@ng-model='selectedEmployee.lastName')]");
    By Update = By.xpath("//button[(@class='main-button' and contains (text(),'Update'))]");
    By Edit = By.id("bEdit");



//**Edit employee class 'Test case' functions
//Get EmployeeName from home page    
	public String Get_FirstEmployee() throws InterruptedException{

		WebDriverWait wait = new WebDriverWait(driver, 1000);
		String Name=wait.until(ExpectedConditions.visibilityOfElementLocated(FirstEmployee)).getText();
		System.out.println("Name"+Name);
		wait.until(ExpectedConditions.visibilityOfElementLocated(FirstEmployee)).click();
		return Name;
    }
	
//Get first employee name from details   
	public String Get_Employee_FirstName_SecondName(){

		WebDriverWait wait = new WebDriverWait(driver, 4000);
		String FirstName=wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_FirstName)).getText();
		String LastName=wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_LastName)).getText();
		String EmployeeName=FirstName+" "+LastName;
		return EmployeeName;
    }
	
//clear first and last employee names from details
	public void Clear_Employee_FirstName_LastName(){

		WebDriverWait wait = new WebDriverWait(driver, 1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_FirstName)).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_LastName)).clear();

    }
//send new employee details and update	
	public void Update() throws FileNotFoundException, IOException{

		WebDriverWait wait = new WebDriverWait(driver, 1000);
		employee.load(new FileReader(new File("employeeData.properties")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_FirstName)).sendKeys(employee.getProperty("firstname"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_LastName)).sendKeys(employee.getProperty("lastname"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Update)).click();
    }
	
//Edit button
	public void Edit(){
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Edit)).click();
    }
	
//Assert after redirect to home page	
	public boolean Displayed_Greetings(){
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(greetings)).isDisplayed();   
    }
}
