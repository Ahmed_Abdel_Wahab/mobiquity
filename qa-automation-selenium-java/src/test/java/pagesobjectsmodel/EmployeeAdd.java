package pagesobjectsmodel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import baseTest.ValidLoginTest;
import utility.PropertyEmployeeManager;

public class EmployeeAdd extends ValidLoginTest{
	
//Load employee data in properties file
	public Properties employee = new Properties();
	
    public WebDriver driver;
    public EmployeeAdd(WebDriver driver) {
        this.driver = driver;
	      	}
    
//**Add Employee elements 
    By AddEmployee= By.id("bAdd");
    By Employee_FirstName = By.xpath("//input[(@ng-model='selectedEmployee.firstName')]");
    By Employee_LastName = By.xpath("//input[(@ng-model='selectedEmployee.lastName')]");
    By Employee_Email=By.xpath("//input[(@ng-model='selectedEmployee.email')]");
    By Employee_StartDate=By.xpath("//input[(@ng-model='selectedEmployee.startDate')]");
    By Employee_Create= By.xpath("//button[(@class='main-button' and contains (text(),'Add'))]");
	By greetings=By.id("greetings");

//***********Add Employee class'TestCas' methods	

//Add employee and create employee data
	public void AddEmployee(){
	    WebDriverWait wait = new WebDriverWait(driver, 1000);
		PropertyEmployeeManager objData;			
		objData = new PropertyEmployeeManager(driver);
//Generate unique firstname and last name at propertyEmployeeManager
		objData.setPropertyParamChanges();
		wait.until(ExpectedConditions.visibilityOfElementLocated(AddEmployee)).click();
    }

//Create Method
	public void Employee_Create() throws FileNotFoundException, IOException{
	    WebDriverWait wait = new WebDriverWait(driver, 1000);
		employee.load(new FileReader(new File("employeeData.properties")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_FirstName)).sendKeys(employee.getProperty("firstname"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_LastName)).sendKeys(employee.getProperty("lastname"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_StartDate)).sendKeys(employee.getProperty("startdate"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_Email)).sendKeys(employee.getProperty("email"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_Create)).click();
    }
//Assert Method
	public boolean Displayed_Greetings(){
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(greetings)).isDisplayed();   
    }
		 
    
}
