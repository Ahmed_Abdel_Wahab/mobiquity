package pagesobjectsmodel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EmployeeViewAndUpdate {
		
    public WebDriver driver;
    public EmployeeViewAndUpdate(WebDriver driver) {
        this.driver = driver;
	      	}
    
	Properties employee = new Properties();
	 
//**Edit Employee elements
     By FirstEmployee = By.cssSelector("li.ng-scope:nth-child(1)");
     By Employee_FirstName = By.xpath("//input[(@ng-model='selectedEmployee.firstName')]");
     By Employee_LastName = By.xpath("//input[(@ng-model='selectedEmployee.lastName')]");
     By Update = By.xpath("//button[(@class='main-button' and contains (text(),'Update'))]");
     By Edit = By.id("bEdit");
     By greetings=By.id("greetings");


//Edit employee class 'Test case' functions
	public String Get_FirstEmployee(){

		WebDriverWait wait = new WebDriverWait(driver, 1000);
		String Name=wait.until(ExpectedConditions.visibilityOfElementLocated(FirstEmployee)).getText();
		System.out.println("Name"+Name);
		wait.until(ExpectedConditions.visibilityOfElementLocated(FirstEmployee)).click();
		return Name;
    }
	
	public void Update() throws FileNotFoundException, IOException{

		WebDriverWait wait = new WebDriverWait(driver, 1000);
		employee.load(new FileReader(new File("employeeData.properties")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_FirstName)).sendKeys(employee.getProperty("firstname"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_LastName)).sendKeys(employee.getProperty("lastname"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(Update)).click();
    }
//clear employee class 'Test case ' functions 
	public void Clear_Employee_FirstName_LastName(){

		WebDriverWait wait = new WebDriverWait(driver, 1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_FirstName)).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_LastName)).clear();

    }

//Edit employee class 'Test case' functions
	public void Edit(){

		Actions action = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		WebElement element=driver.findElement(FirstEmployee);
		//Double click
		action.doubleClick(element).perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(Employee_FirstName));
    }

	//Assert after redirect to home page	
		public boolean Displayed_Greetings(){
			WebDriverWait wait = new WebDriverWait(driver, 1000);
			return wait.until(ExpectedConditions.visibilityOfElementLocated(greetings)).isDisplayed();   
	    }
}
