package pagesobjectsmodel;
import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EmployeeLogout {
		
    public WebDriver driver;
    public EmployeeLogout(WebDriver driver) {
        this.driver = driver;
	      	}

//Logout employee elements
     By Logout=By.xpath("//p[(@class='main-button' and contains (text(),'Logout'))]");
     By user = By.xpath("//input[(@ng-model='user.name')]");
     By greetings=By.id("greetings");

//Logout employee class 'Test case' methods
	public void Logout(){
		WebDriverWait wait = new WebDriverWait(driver, 2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Logout)).click();
    }
	public boolean IsUserDisplayed(){
		
		WebDriverWait wait = new WebDriverWait(driver, 2000);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(user)).isDisplayed();   
	}
	
}
