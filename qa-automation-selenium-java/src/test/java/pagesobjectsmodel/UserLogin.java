package pagesobjectsmodel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserLogin {
		
    public WebDriver driver;
    public UserLogin(WebDriver driver) {
        this.driver = driver;
	      	}
	Properties properties = new Properties();

	By username=By.xpath("//input[(@ng-model='user.name')]");	
	By password=By.xpath("//input[(@ng-model='user.password')]");
	By login= By.xpath("//button[(@class='main-button' and contains (text(),'Login'))]");

	public By greetings=By.id("greetings");
	
	public void Login() throws FileNotFoundException, IOException{
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		properties.load(new FileReader(new File("loginData.properties")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(username)).sendKeys(properties.getProperty("username"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(password)).sendKeys(properties.getProperty("password"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(login)).click();
	}
	
	public String Get_Greetings(){

		String last=driver.findElement(greetings).getText();
		return last;
    }
	public boolean Displayed_Greetings(){

		return driver.findElement(greetings).isDisplayed();   
    }
}
