package utility;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class PreConditionSetup {
    public static WebDriver driver;
	public Properties properties = new Properties();
	public Properties employee = new Properties();
	pagesobjectsmodel.UserLogin objPom;
	pagesobjectsmodel.EmployeeAdd objEmployeeAdd;
	
	public void setUpBeforeTestClass() throws Exception {
//Chrome driver added to project 'resource file'
		System.setProperty("webdriver.chrome.driver", "Resources/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
//Load login data provided at loginData.properties file
		properties.load(new FileReader(new File("loginData.properties")));
//Load employee data provided at employeeData.properties file
		employee.load(new FileReader(new File("employeeData.properties")));
     	driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.get("http://cafetownsend-angular-rails.herokuapp.com/login");

	}
	
	public void tearDownAfterTestClass() throws Exception {

		driver.quit();
	}
	
	
}
