package utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.openqa.selenium.WebDriver;

public class PropertyEmployeeManager extends PreConditionSetup {
		

	public PropertyEmployeeManager(WebDriver driver) {
		PreConditionSetup.driver = driver;
		}

	public void setPropertyParamChanges() {
	    try {
//Set employee data using predefined data at employeeData.properties and add unique 5 digit integer to it
			long number = (long) Math.floor(Math.random() * 90000L) + 10000L;
			String FirstName="FirstName"+number;
			String LastName="LastName"+number;
			String startdate="1999-01-05";
			String email="test"+number+"@gmail.com";
	        employee.setProperty("firstname", FirstName.toString());
	        employee.setProperty("lastname", LastName.toString());
	        employee.setProperty("startdate", startdate.toString());
	        employee.setProperty("email", email.toString());

	        File f = new File("employeeData.properties");
	        OutputStream out = new FileOutputStream( f );
	        employee.store(out, "This is an optional header comment string");
	    }
	    catch (Exception e ) {
	        e.printStackTrace();
	    }
	}
	
	
}
