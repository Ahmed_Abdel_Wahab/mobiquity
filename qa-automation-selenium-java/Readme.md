# Project Assessment:
The provided repository contains an complete automated test for testing cafe application features

The test is supposed to:
1. The application allows you to login with a username and a password 

2. The application displays the list of employees in a caf�. Details about each employee can be viewed by double clicking on the employee name 

3. Employees can be added to the list of employees by adding their first and last names, start date (format YYYY-MM-DD) and email 

4. Employee data can be changed by clicking on Edit
 
5. Employee data can be deleted by clicking on Delete 

6. Users can logout from the application 

# Deliverables 
-Folder named : mobiquity_Deliverables which is included in project folder 'qa-automation-selenium-java' contains : 'TestCases.xlsx'"Test cases for this application" 

-'Documentation.pdf' "Contains test coverage, approach , classes structure ,Used Automation Framework, automation build , Test results " 


# Notes
- Update the loginData.properties file and employeeData.properties to replace dummy credentials before you run the tests. 

- I haven't included downloaded packages or auto-generated folders in my submission except chrome driver at resources folders.

# Tasks:
- i have implemented all the features test cases
 
# Prerequisites:
- BitBucket: git clone https://Ahmed_Abdel_Wahab@bitbucket.org/Ahmed_Abdel_Wahab/mobiquity.git
- ChromeDriver 'already included', JDK 8+
- Any IDE

# Development Environment/how to launch the automated tests:
-Clone project from BitBucket using: git clone https://Ahmed_Abdel_Wahab@bitbucket.org/Ahmed_Abdel_Wahab/mobiquity.git

- Modify PreConditionSetup to point to ChromeDriver's path on your system if you don't want to use another driver instead of the drive i have already included.

- On any terminal, move to the project's root folder and execute the following command:
    - ./gradlew clean test ,or at windows ' gradlew clean test
That will execute all test cases for application features

# Documentation for automation tests:
1)This is a gradle project consist of 4 packages that represent :
-baseTest which includes validLoginTest that represent login test case that will become a precondition for any other test case.

-pagesobjectsmodel which includes POM that contains all paths , and common functions that will be used by all other classes 'test cases' 

-utility which includes 'PreconditionSetup' and 'PropertyEmployeeManager' both used to do certain operation : driver intialize , write and read from files and driver tear down

-mobiquity.e2e includes all test that covers application features which are:

*ValidLoginTest: that test ability to login using provided test data at loginData.employee file and verifying that and assert if true.

*AddEmployeeTest : that test ability to  add employee using test data that are generated at property manager and saved in employeeData.properties to be used later to verify .

*DeleteEmployeeTest: test ability to delete employee and verify that.

*EditEmployeeTest: test ability to edit employee using double-click by selecting first employee 
in the list and get his full name then edit it and verify his firstname and lastname 

*ViewEmployeeAndUpdateDetails:test edit employee then update his info in addition to verifying that

*Logout: test logout functionality and verifying it

# Execution order and options to run tests:

1)Navigate to project path at ' ../qa-automation-selenium-java/ ' then launch command prompt.

2)To run all 'mobiquity.e2e' package class tests ---> gradlew clean test
-That will execute all classes 'test cases' in that package

-To Run specific class or test case : >gradlew test --tests *'ClassName'

-For ex: >gradlew test --tests *AddEmployeeTest

3)Execution order based on their order at package which are 'AddEmployee , DeleteEmployee , EditEmployee,LogoutTest,ViewEmployeeAndUpdateDetails' and validlogin test as a precondition for these test cases 

